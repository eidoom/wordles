#!/usr/bin/env python

import dominate
from dominate import tags
from dominate.util import raw
import yaml

langs = {}

def setup():
    languages = list(yaml.load_all(open('database/languages.yaml'), Loader=yaml.SafeLoader))[0]
    mlwordles = list(yaml.load_all(open('database/mlwordles.yaml'), Loader=yaml.SafeLoader))[0]
    domwordles = list(yaml.load_all(open('database/domain.yaml'), Loader=yaml.SafeLoader))[0]
    twistwordles = list(yaml.load_all(open('database/twist.yaml'), Loader=yaml.SafeLoader))[0]
    reimplwordles = list(yaml.load_all(open('database/reimplementations.yaml'), Loader=yaml.SafeLoader))[0]
    nonlingwordles = list(yaml.load_all(open('database/nonlinguistic.yaml'), Loader=yaml.SafeLoader))[0]
    miscwordles = list(yaml.load_all(open('database/misc.yaml'), Loader=yaml.SafeLoader))[0]
    nonwordles = list(yaml.load_all(open('database/non-wordly.yaml'), Loader=yaml.SafeLoader))[0]
    media = list(yaml.load_all(open('database/media.yaml'), Loader=yaml.SafeLoader))[0]
    lists = list(yaml.load_all(open('database/lists.yaml'), Loader=yaml.SafeLoader))[0]
    global langs
    for language in languages:
        langs[language['iso']] = {
            'name': language['native']['name'],
            'name-en': language['english']['name'],
            }
        if 'css' in language['native']:
            langs[language['iso']]['css'] = language['native']['css']
        if 'wiki' in language['native']:
            langs[language['iso']]['wiki'] = language['native']['wiki']['title']
            langs[language['iso']]['wiki-lang'] = language['native']['wiki']['lang']
        if 'wiki' in language['english']:
            langs[language['iso']]['wiki-en'] = language['english']['wiki']
    return mlwordles, domwordles, twistwordles, reimplwordles, nonlingwordles, miscwordles, media, nonwordles, lists

def header(doc):
    with doc.head:
        #  link(rel='stylesheet', href='mini-default.min.css')
        #  link(rel='stylesheet', href='pure-min.css')
        tags.link(rel='stylesheet', href="https://unpkg.com/purecss@2.0.6/build/pure-min.css", integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5", crossorigin="anonymous")
        tags.meta(name="viewport", content="width=device-width, initial-scale=1")
        tags.link(rel='stylesheet', href='style.css')
        raw('''
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Gentium+Basic:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
                ''')
        tags.script(src="https://twemoji.maxcdn.com/v/latest/twemoji.min.js", crossorigin="anonymous")
        tags.script(src="https://code.jquery.com/jquery-git.js")
        tags.link(rel='icon', href='gfx/logo.png')
        tags.meta(name="description", content="An ever-growing multilingual list of Wordle-like games from all over the world 🟩🟩🟩🟩🟩")
        tags.meta(name="keywords", content="Wordle, game, list, multilingual, word, games, multilingualism, words")
        tags.meta(name="author", content="Júda Ronén")
        tags.meta(property="og:title", content="🟩 Wordles of the World")
        tags.meta(property="og:type", content="website")
        tags.meta(property="og:image", content="https://rwmpelstilzchen.gitlab.io/wordles/gfx/wordle/wordle.png")
        tags.meta(property="og:url", content="https://rwmpelstilzchen.gitlab.io/wordles/")
        tags.meta(property="og:description", content="An ever-growing multilingual list of Wordle-like games from all over the world 🟩🟩🟩🟩🟩")
        tags.meta(name="twitter:card", content="summary_large_image")
        tags.meta(name="twitter:creator", content="@judaronen")

def toptext(mlwordles, domwordles, twistwordles, reimplwordles, nonlingwordles, miscwordles, nonwordles):
    global langs
    with tags.h1(__pretty=False):
        tags.span("Wordles of the World", cls="title")
        tags.br()
        for letter in 'unite':
            tags.div(letter, cls="tile")
    with tags.p(__pretty=False):
        tags.span("This is — to the best of our knowledge — the most comprehensive list of ")
        tags.a("Wordle", href="https://www.powerlanguage.co.uk/wordle/")
        tags.span("-like games and resources online, with ")
        tags.span(str(sum(len(s) for s in [mlwordles, domwordles, twistwordles, reimplwordles, nonlingwordles, miscwordles, nonwordles])))
        tags.span(" entries in ")
        tags.span(str(len(langs)-1))
        tags.span(" languages (")
        tags.a(str(8549-len(langs)-1), href="https://glottolog.org/glottolog/language")
        tags.span(" to go…). ")
        tags.span("Its goal is threefold: having fun, celebrating linguistic diversity, and exploring playful creativity.")
    with tags.div(cls="pure-menu pure-menu-horizontal pure-menu-scrollable"):
        with tags.ul(cls="pure-menu-list"):
            #  tags.span("hi", cls="pure-menu-heading")
            with tags.li(cls="pure-menu-item pure-button"):
                tags.a("🌐 multilingual", cls="pure-menu-link", href="#ml")
            with tags.li(cls="pure-menu-item pure-button"):
                tags.a("🔍 domain-specific", cls="pure-menu-link", href="#domain")
            with tags.li(cls="pure-menu-item pure-button"):
                tags.a("🃏 twist", cls="pure-menu-link", href="#twist")
            with tags.li(cls="pure-menu-item pure-button"):
                tags.a("♻ clones", cls="pure-menu-link", href="#clones")
            with tags.li(cls="pure-menu-item pure-button"):
                tags.a("😶 non-linguistic", cls="pure-menu-link", href="#nonling")
            with tags.li(cls="pure-menu-item pure-button"):
                tags.a("⚙ misc", cls="pure-menu-link", href="#misc")
            with tags.li(cls="pure-menu-item pure-button"):
                tags.a("🟨 non-wordly", cls="pure-menu-link", href="#nonwordly")
    with tags.p(__pretty=False):
        tags.span("This list is collaborative and is based on a number of sources: ")
        tags.a("other lists", href="#lists")
        tags.span(", ")
        tags.a("merge requests", href="https://gitlab.com/rwmpelstilzchen/wordles/-/merge_requests?scope=all&state=all")
        tags.span(", and entries suggested publicly (e.g. ")
        tags.a("here", href="https://twitter.com/JudaRonen/status/1484274702273105921")
        tags.span(", by ")
        tags.a("@jaaaarwr", href="https://twitter.com/jaaaarwr")
        tags.span(" et al., and ")
        tags.a("here", href="https://twitter.com/JudaRonen/status/1485640320201203712/")
        tags.span(") and privately (emails, DM, Telegram, etc.), as well as entries found on web search (on ")
        tags.a("Twitter", href="https://twitter.com/yeahwhyyes/status/1486040873980866562")
        tags.span(" and elsewhere). ")
        tags.span("Code repository is available on ")
        with tags.a(href="https://gitlab.com/rwmpelstilzchen/wordles", __pretty=False):
            tags.span("GitLab ")
            tags.img(src='gfx/code/gitlab.svg', cls='icon')
        tags.span(". The list and its code (")
        tags.em("sans")
        tags.span(" various logos, emoji, fonts, etc. not created by this project) are in the ")
        with tags.a(href="https://en.wikipedia.org/wiki/Public_domain"):
            tags.span("public domain ")
            tags.img(src='gfx/pd.svg', cls='icon')
        tags.span(" under ")
        with tags.a(href="https://creativecommons.org/share-your-work/public-domain/cc0/"):
            tags.span("CC0 ")
            tags.img(src='gfx/cc0.svg', cls='icon')
        tags.span(".")
    with tags.p(__pretty=False):
        tags.span("Read more about multilingual and remixable Wordles on ")
        tags.a("Rest of World", href="https://restofworld.org/2022/wordle-viral-turkish-japanese-tamil-portuguese/")
        tags.span(", ")
        tags.a("Duolingo", href="https://blog.duolingo.com/wordle-in-other-languages/")
        tags.span(", ")
        tags.a("Wikipedia", href="https://en.wikipedia.org/wiki/Wordle#Adaptations_and_clones")
        tags.span(", ")
        tags.a("GlitchBlog", href="https://blog.glitch.com/post/wordle-mania-and-the-remixable-web")
        tags.span(" and the Guardian (")
        tags.a("here", href="https://www.theguardian.com/games/2022/feb/05/urdu-chinese-even-old-norse-how-wordle-spread-across-the-globe")
        tags.span(" and ")
        tags.a("here", href="https://www.theguardian.com/games/2022/feb/06/worried-about-losing-wordle-here-are-some-alternatives-just-in-case")
        tags.span("). Wordles of the World has been featured on ")
        tags.a("media", href="#media")
        tags.span(".")

    with tags.h2(__pretty=False, id="boost"):
        for letter in 'boost':
            tags.div(letter, cls="tile")
    with tags.table(cls='pure-table participate', id='participate'):
        with tags.tbody():
            with tags.tr():
                tags.td("📣")
                with tags.td(__pretty=False):
                            tags.span("Share this list with your friends. If you use Twitter, you can retweet ")
                            tags.a("this tweet", href="https://twitter.com/JudaRonen/status/1485640320201203712/")
                            tags.span(". The more people it reaches, the more people may benefit from it and possibly suggest new entries.")
            with tags.tr():
                tags.td("➕")
                with tags.td():
                    tags.span("Do you know of any entry that is absent from the list? ")
                    tags.a("Contact us", href="#contact")
                    tags.span(" (preferably by making a merge request) and we will be happy to add it 🙂.")
            with tags.tr():
                tags.td("💡")
                with tags.td(__pretty=False):
                    tags.span("If you want to make a version for your language, you can follow ")
                    tags.a("these instructions", href="https://blog.mothertongues.org/wordle/")
                    tags.span(" or ")
                    tags.a("these", href="https://github.com/cwackerfuss/react-wordle#how-can-i-create-a-version-in-another-language")
                    tags.span(" (requires basic technical know-how and a good wordlist). Consider making it ")
                    tags.a("accessible", href="https://twitter.com/JudaRonen/status/1486992955227480071")
                    tags.span(".")
            with tags.tr():
                tags.td("🕸")
                with tags.td():
                    tags.span("If you have already made a wordle in any language, linking")
                    tags.a("back here", href="https://rwmpelstilzchen.gitlab.io/wordles/")
                    tags.span("would be great, as it would create a network of wordles.")

    with tags.h2(__pretty=False, id="write"):
        for letter in 'write':
            tags.div(letter, cls="tile")
    with tags.table(cls='pure-table contact', id='contact'):
        with tags.tbody():
            with tags.tr():
                tags.td(tags.img(src='gfx/code/gitlab.svg', cls='icon'))
                with tags.td(__pretty=False):
                    tags.span("If you want to suggest an edit on the ")
                    tags.a("GitLab repository", href="https://gitlab.com/rwmpelstilzchen/wordles")
                    tags.span(", please ")
                    tags.a("fork it", href="https://gitlab.com/rwmpelstilzchen/wordles")
                    tags.span(" and ")
                    tags.a("make a merge request", href="https://gitlab.com/rwmpelstilzchen/wordles/-/merge_requests")
                    tags.span(", or ")
                    tags.a("open an issue", href="https://gitlab.com/rwmpelstilzchen/wordles/-/issues")
                    tags.span(".")
            with tags.tr():
                tags.td("👩‍💻")
                with tags.td(__pretty=False):
                    tags.span("If you don’t want to use Git, or don’t know how to, feel free to contact the coordinator — Júda Ronén — ")
                    tags.a("through other ways of communication", href="http://me.digitalwords.net/")
                    tags.span(", in Hebrew, Welsh, Norwegian, Esperanto or English.")

def tab_lang_eng(wordle):
    with tags.td():
        with tags.span(cls="directlink"):
            tags.a("🔗",
                    href="#"+wordle['lang'])
            tags.span(" ")
        if 'wiki-en' in langs[wordle['lang']]:
            tags.a(
                    langs[wordle['lang']]['name-en'],
                    href="https://en.wikipedia.org/wiki/" + langs[wordle['lang']]['wiki-en'],
                    cls="lowkeylink",
                    id=wordle['lang']
                    )
        else:
            tags.span(
                    langs[wordle['lang']]['name-en'],
                    id=wordle['lang']
                    )

def tab_lang_native(wordle):
    if 'css' in langs[wordle['lang']]:
        if 'wiki' in langs[wordle['lang']]:
            tags.td(tags.a(
                langs[wordle['lang']]['name'],
                href="https://" + langs[wordle['lang']]['wiki-lang'] + ".wikipedia.org/wiki/" + langs[wordle['lang']]['wiki'],
                cls="lowkeylink"
                ), cls=langs[wordle['lang']]['css'])
        else:
            tags.td(langs[wordle['lang']]['name'], cls=langs[wordle['lang']]['css'])
    else:
        if 'wiki' in langs[wordle['lang']]:
            tags.td(tags.a(
                langs[wordle['lang']]['name'],
                href="https://" + langs[wordle['lang']]['wiki-lang'] + ".wikipedia.org/wiki/" + langs[wordle['lang']]['wiki'],
                cls="lowkeylink"
                ))
        else:
            tags.td(langs[wordle['lang']]['name'])

def tab_lang(wordle, prevlang):
    global langs
    if prevlang != wordle['lang']:
        tab_lang_eng(wordle)
        tab_lang_native(wordle)
    else:
        tags.td()
        tags.td()

def tab_lang_only_english(wordle, prevlang):
    global langs
    if prevlang != wordle['lang']:
        tab_lang_eng(wordle)
    else:
        tags.td()

def tab_wordle(wordle):
    if 'css' in wordle:
        tags.td(tags.a(raw(wordle['name']), href=wordle['url']), cls=wordle['css'])
    else:
        tags.td(tags.a(wordle['name'], href=wordle['url']))

def tab_code(wordle):
    if 'src' in wordle:
        codeicons = {
            'github': 'github.svg',
            'git':    'git.svg',
            'glitch': 'glitch.svg',
            'gitlab': 'gitlab.svg',
            'pico-8': 'pico-8.png',
            'other':  'code.svg',
        }
        tags.td(tags.a(tags.img(src="gfx/code/"+codeicons[wordle['src-type']], cls='icon'), href=wordle['src']))
    else:
        tags.td()

def tab_note(wordle):
    if 'note' in wordle:
        tags.td(raw(wordle['note']))
    else:
        tags.td()

def tab_media(wordle):
    if 'media' in wordle:
        with tags.td():
            for medium in wordle['media']:
                tags.a("🔗", href=medium['url'])
    else:
        tags.td()

def mltable(wordles):
    global langs
    with tags.table(cls='pure-table mltable', id="ml"):
        tags.caption("🌐 Multilingual vanilla wordles; " + str(len(wordles)) + " entries")
        with tags.thead():
            with tags.tr():
                with tags.th(colspan='2'):
                    tags.span("Language")
                    tags.span("🔗",
                            onclick="$('.directlink').toggle();",
                            style="float: right;")
                tags.th("Name")
                tags.th("Code")
                tags.th("Note")
                tags.th("Media")
        prevlang = ''
        with tags.tbody():
            for wordle in wordles:
                with tags.tr():
                    tab_lang(wordle, prevlang)
                    prevlang = wordle['lang']
                    tab_wordle(wordle)
                    tab_code(wordle)
                    tab_note(wordle)
                    tab_media(wordle)

def eng_domain_specific(wordles):
    with tags.table(cls='pure-table domaintable', id="domain"):
        tags.caption("🔍 Domain-specific wordles; " + str(len(wordles)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th("Language")
                tags.th("Name")
                tags.th("Code")
                tags.th("Emoji")
                tags.th("Domain")
                tags.th("Media")
        prevlang = ''
        with tags.tbody():
            for wordle in wordles:
                with tags.tr():
                    tab_lang_only_english(wordle, prevlang)
                    prevlang = wordle['lang']
                    tab_wordle(wordle)
                    tab_code(wordle)
                    tags.td(wordle['emoji'])
                    tags.td(raw(wordle['domain']))
                    tab_media(wordle)

def eng_twist(wordles):
    with tags.table(cls='pure-table twisttable', id="twist"):
        tags.caption("🃏 Wordles with a gameplay twist or variation; " + str(len(wordles)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th("Language")
                tags.th("Name")
                tags.th("Code")
                tags.th("Emoji")
                tags.th("Variation")
                tags.th("Media")
        prevlang = ''
        with tags.tbody():
            for wordle in wordles:
                with tags.tr():
                    tab_lang_only_english(wordle, prevlang)
                    prevlang = wordle['lang']
                    tab_wordle(wordle)
                    tab_code(wordle)
                    tags.td(wordle['emoji'])
                    tags.td(raw(wordle['twist']))
                    tab_media(wordle)

def eng_reimplementations(wordles):
    with tags.table(cls='pure-table reimplementationtable', id="clones"):
        tags.caption("♻ Reimplementations of Wordle (English); " + str(len(wordles)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th("Name")
                tags.th("Code")
                tags.th("Interface")
                tags.th("Development")
                tags.th("Note")
        with tags.tbody():
            for wordle in wordles:
                with tags.tr():
                    tab_wordle(wordle)
                    tab_code(wordle)
                    tags.td(raw(wordle['interface']))
                    tags.td(raw(wordle['dev']))
                    tab_note(wordle)

def nonling(wordlesses):
    with tags.table(cls='pure-table nonlinguistictable', id="nonling"):
        tags.caption("😶 Non-linguistic wordles(s); " + str(len(wordlesses)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th("Name")
                tags.th("Code")
                tags.th("Emoji")
                tags.th("Object of guessing")
                tags.th("Media")
        with tags.tbody():
            for wordless in wordlesses:
                with tags.tr():
                    tab_wordle(wordless)
                    tab_code(wordless)
                    tags.td(wordless['emoji'])
                    tags.td(raw(wordless['object']))
                    tab_media(wordless)

def nonwordly(nonwordles):
    with tags.table(cls='pure-table nonwordlytable', id="nonwordly"):
        tags.caption("🟨 Related games with non-Wordle-like rules; " + str(len(nonwordles)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th("Name")
                tags.th("Code")
                tags.th("Emoji")
                tags.th("Object")
                tags.th("Feedback")
                tags.th("Note")
                tags.th("Media")
        with tags.tbody():
            for wordle in nonwordles:
                with tags.tr():
                    tab_wordle(wordle)
                    tab_code(wordle)
                    tags.td(wordle['emoji'])
                    tags.td(raw(wordle['object']))
                    tags.td(raw(wordle['feedback']))
                    tab_note(wordle)
                    tab_media(wordle)


def media(media):
    with tags.table(cls='pure-table mediatable', id="media"):
        tags.caption("📰 Media coverage of Wordles of the World; " + str(len(media)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th("Publication")
                tags.th("Title")
                tags.th("Type")
                tags.th("Date")
        with tags.tbody():
            for medium in media:
                with tags.tr():
                    with tags.td():
                        tags.a(medium['publication']['name'],
                                href=medium['publication']['url'])
                    with tags.td():
                        tags.a(medium['title'],
                                href=medium['url'])
                    tags.td(medium['type'])
                    tags.td(str(medium['date']))

def lists(lists):
    with tags.table(cls='pure-table liststable', id="lists"):
        tags.caption("📜 Other lists of Wordle-like games; " + str(len(lists)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th("Coordinator")
                tags.th("Title")
                tags.th("Note")
                tags.th("Date")
        with tags.tbody():
            for lst in lists:
                with tags.tr():
                    with tags.td():
                        tags.a(lst['coordinator']['name'],
                                href=lst['coordinator']['url'])
                    with tags.td():
                        tags.a(lst['title'],
                                href=lst['url'])
                    tab_note(lst)
                    tags.td(str(lst['date']))

def misc(wordles):
    with tags.table(cls='pure-table misctable', id="misc"):
        tags.caption("⚙ Tools and miscellanea; " + str(len(wordles)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th("Name")
                tags.th("Code")
                tags.th("Emoji")
                tags.th("Description")
                tags.th("Media")
        with tags.tbody():
            for wordle in wordles:
                with tags.tr():
                    tab_wordle(wordle)
                    tab_code(wordle)
                    tags.td(wordle['emoji'])
                    tags.td(raw(wordle['description']))
                    tab_media(wordle)

def bottomtext():
    with tags.div(style="margin-top: 3em; text-align: center"):
        tags.img(src="gfx/wordle/wordle.svg", style="width: 80%")
        with tags.p(__pretty=False):
            tags.span("A ")
            tags.a("word cloud", href="https://en.wikipedia.org/wiki/Tag_cloud")
            tags.span(" (a.k.a. ")
            tags.em("wordle")
            tags.span(") made with ")
            tags.a("Jason Davies", href="https://www.jasondavies.com/")
            tags.span("’s open source")
            tags.a(" generator", href="https://www.jasondavies.com/wordcloud/")
            tags.span(".")

def main():
    mlwordles, domwordles, twistwordles, reimplwordles, nonlingwordles, miscwordles, mediadata, nonwordles, listsdata = setup()
    doc = dominate.document(title='Wordles of the World')
    header(doc)
    with doc.body:
        with tags.div(cls='content-wrapper'):
            with tags.div(cls='content'):
                toptext(mlwordles, domwordles, twistwordles, reimplwordles, nonlingwordles, miscwordles, nonwordles)
                tags.hr(style="margin-top: 3em; border: 0px; border-top: 1px solid #d3d6da")
                mltable(mlwordles)
                eng_domain_specific(domwordles)
                eng_twist(twistwordles)
                with tags.p():
                    tags.span("When a version is tagged ‘fraudulent’ it means it seems like trying to trick people into thinking it is the original Wordle. If we are wrong, ")
                    tags.a("drop us a note", href="#contact")
                    tags.span(" and we will remove the tag.")
                eng_reimplementations(reimplwordles)
                nonling(nonlingwordles)
                misc(miscwordles)
                nonwordly(nonwordles)
                lists(listsdata)
                media(mediadata)
                bottomtext()
    with doc:
        tags.script("twemoji.parse(document.body);")
    print(doc)

if __name__=="__main__":
    main()
